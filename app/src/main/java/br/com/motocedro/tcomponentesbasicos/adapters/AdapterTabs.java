package br.com.motocedro.tcomponentesbasicos.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.motocedro.tcomponentesbasicos.fragments.Fragment1;
import br.com.motocedro.tcomponentesbasicos.fragments.Fragment2;

/**
 * Created by loopback on 19/11/15.
 */
public class AdapterTabs extends FragmentPagerAdapter {


    private Context context;
    static final int QTD_TABS = 2;
    private static final String[] tabTitles= {"Back IMG","GridView"};

    public AdapterTabs(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        if(position == 0){
            Fragment1 mFragment1 = new Fragment1();
            return mFragment1;
        }else{
            Fragment2 mFragment2 = new Fragment2();
            return mFragment2;
        }

    }

    @Override
    public int getCount() {
        return QTD_TABS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

}
