package br.com.motocedro.tcomponentesbasicos.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.motocedro.tcomponentesbasicos.R;
import br.com.motocedro.tcomponentesbasicos.modelos.ModeloLista;

/**
 * Created by loopback on 18/11/15.
 */
public class AdapterLista extends ArrayAdapter<ModeloLista> {

    private List<ModeloLista> lista;
    private Activity context;

    public AdapterLista(Activity context, List<ModeloLista> lista){
        super(context, R.layout.modelo_lista, lista);
        this.lista = lista;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater layoutInflater = context.getLayoutInflater();
            convertView = layoutInflater.inflate(R.layout.modelo_lista, null);
        }

        ModeloLista modelo = lista.get(position);

        TextView mensagem = (TextView) convertView.findViewById(R.id.txtMensagem);
        TextView data = (TextView) convertView.findViewById(R.id.txtData);

        mensagem.setText(modelo.mensagem);
        data.setText(modelo.date.toString());

        return convertView;
    }
}
