package br.com.motocedro.tcomponentesbasicos.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import br.com.motocedro.tcomponentesbasicos.R;

/**
 * Created by loopback on 18/11/15.
 */
public class AdapterGridViewFrag extends BaseAdapter{
        private Context mContext;
        private List<ImageView> imagens;

        public AdapterGridViewFrag(Context c) {
            mContext = c;
            imagens = new ArrayList<>();
        }

        public int getCount() {
            return mThumbIds.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(8, 8, 8, 8);
            } else {
                imageView = (ImageView) convertView;
            }

            imageView.setImageResource(mThumbIds[position]);
            imagens.add(imageView);
            return imageView;
        }

        // references to our images
        private Integer[] mThumbIds = {
                android.R.drawable.alert_dark_frame, android.R.drawable.alert_dark_frame,
                android.R.drawable.alert_dark_frame, android.R.drawable.alert_dark_frame,
                android.R.drawable.alert_dark_frame, android.R.drawable.alert_dark_frame,
                android.R.drawable.alert_dark_frame, android.R.drawable.alert_dark_frame,
                android.R.drawable.alert_dark_frame, android.R.drawable.alert_dark_frame,
                android.R.drawable.alert_dark_frame, android.R.drawable.alert_dark_frame,
                android.R.drawable.alert_dark_frame, android.R.drawable.alert_dark_frame
        };
    }
