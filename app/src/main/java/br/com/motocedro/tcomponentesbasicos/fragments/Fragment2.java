package br.com.motocedro.tcomponentesbasicos.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import br.com.motocedro.tcomponentesbasicos.R;
import br.com.motocedro.tcomponentesbasicos.adapters.AdapterGridViewFrag;

/**
 * Created by loopback on 18/11/15.
 */
public class Fragment2 extends Fragment{

    private GridView gridView;

    //GRIDVIEW
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment2, container, false);

        gridView = (GridView) view.findViewById(R.id.gridview);

        gridView.setAdapter(new AdapterGridViewFrag(getActivity()));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "" + position,
                        Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }
}
