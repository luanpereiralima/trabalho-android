package br.com.motocedro.tcomponentesbasicos.activities;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.motocedro.tcomponentesbasicos.adapters.AdapterLista;
import br.com.motocedro.tcomponentesbasicos.modelos.ModeloLista;
import br.com.motocedro.tcomponentesbasicos.R;

public class MainActivity extends AppCompatActivity {

    private ToggleButton toggleButton;
    private ListView lista;

    //REQUEST CODES
    private static int REQUEST_CODE_ACT3 = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //MUSIC
        final MediaPlayer musica = MediaPlayer.create(this, R.raw.brasileiro);

        //TOGGLE BUTTON
        toggleButton = (ToggleButton)findViewById(R.id.btMusica);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //MUSIC
                if (isChecked)
                    musica.start();
                else{
                    musica.pause();
                }
            }
        });

        //LISTA
        lista = (ListView) findViewById(R.id.lvLista);

        //ADAPTER
        lista.setAdapter(new AdapterLista(this, getModelos()));

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0: {
                        Intent i = new Intent(MainActivity.this, Activity2.class);
                        i.putExtra("extra", "Mensagem para Activity 2");
                        startActivity(i);
                        break;
                    }
                    case 1:
                        startActivityForResult(new Intent(MainActivity.this, Activity3.class), REQUEST_CODE_ACT3);
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_ACT3)
            Toast.makeText(this, "RETORNOU O CODIGO: "+requestCode, Toast.LENGTH_SHORT).show();
    }

    //LIST
    private List<ModeloLista> getModelos(){
        List<ModeloLista> lista = new ArrayList<>();

        ModeloLista modelo = new ModeloLista();

        modelo.mensagem = "Activity 2, com parametros.";
        modelo.date = new Date();

        lista.add(modelo);

        modelo = new ModeloLista();

        modelo.mensagem = "Activity 3, com OnActivityResult.";
        modelo.date = new Date();

        lista.add(modelo);

        return lista;
    }

    //MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_multiplas_telas:
                startActivity(new Intent(this, ActivitiesTabs.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
