package br.com.motocedro.tcomponentesbasicos.activities;

import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import br.com.motocedro.tcomponentesbasicos.R;

/**
 * Created by loopback on 18/11/15.
 */
public class Activity3 extends AppCompatActivity {
    static int result = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);


        RadioGroup group = (RadioGroup) findViewById(R.id.radioGroup);
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton) {
                    result = 1;
                } else if (checkedId == R.id.radioButton2) {
                    result = 2;
                } else if (checkedId == R.id.radioButton3) {
                    Log.i("teste", "entrou akii");
                    result = 3;
                }
            }
        });


       final  Button button = (Button) findViewById(R.id.btEnviar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("teste", result + "");
                setResult(result);
                finish();
            }
        });

        final EditText editText = (EditText) findViewById(R.id.edtAc);
        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                button.setText(editText.getText());
                return false;
            }
        });

    }
}
