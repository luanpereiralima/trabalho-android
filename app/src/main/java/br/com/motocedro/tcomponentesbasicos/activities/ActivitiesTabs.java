package br.com.motocedro.tcomponentesbasicos.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import br.com.motocedro.tcomponentesbasicos.R;
import br.com.motocedro.tcomponentesbasicos.adapters.AdapterTabs;

/**
 * Created by loopback on 18/11/15.
 */
public class ActivitiesTabs extends AppCompatActivity {

    //ACTIVITY COM TAB
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPagerTabs);
        viewPager.setOffscreenPageLimit(2);

        AdapterTabs tabAdapter = new AdapterTabs(getSupportFragmentManager(), this);
        viewPager.setAdapter(tabAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
    }
}
