package br.com.motocedro.tcomponentesbasicos.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.motocedro.tcomponentesbasicos.R;

/**
 * Created by loopback on 18/11/15.
 */
public class Fragment1 extends Fragment{

    //IMAGEM BACKGROUND
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment1, container, false);
        return view;
    }
}
